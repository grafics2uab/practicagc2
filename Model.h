#ifndef _MODEL_H
#define _MODEL_H

#include "Util.h"

class Model
{
public:
	Model();
	~Model();
	void Dibuixa();
    void Eixos();

};
#endif /* _MODEL_H */
