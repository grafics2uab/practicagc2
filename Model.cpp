#include "Model.h"
#include <QImage>
#include <QDebug>
#include <QGLWidget>

Model::Model(){

}

Model::~Model(){
	
}

void Model::Dibuixa(){

    glColor3f(1.0,1.0,1.0);
    glPushMatrix();
        glRotatef(90,1.0,0.0,0.0);
        glFrontFace(GL_CW);
        glutSolidTeapot(.3);
        glFrontFace(GL_CCW);
    glPopMatrix();

}

void Model::Eixos(){

    glDisable(GL_LIGHTING);

    // Eix X (vermell)
    glColor3f(1.0,0.0,0.0);
    glBegin(GL_LINES);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(300.0,0.0,0.0);
    glEnd();

    // Eix Y (verd)
    glColor3f(0.0,1.0,0.0);
    glBegin(GL_LINES);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.0,300.0,0.0);
    glEnd();

    // Eix Z (blau)
    glColor3f(0.0,0.0,1.0);
    glBegin(GL_LINES);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.0,0.0,300.0);
    glEnd();

    glEnable(GL_LIGHTING);

}
