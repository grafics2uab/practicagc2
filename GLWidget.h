#ifndef _GLWIDGET_H
#define _GLWIDGET_H

#include "Util.h"
#include <QtOpenGL/QGLWidget>
#include <QGLShaderProgram>
#include "Model.h"
#include "Camera.h"
#include "Illumination.h"

class GLWidget : public QGLWidget {

    Q_OBJECT // must include this if you use Qt signals/slots

public:
    GLWidget(QWidget *parent = NULL);
    ~GLWidget();

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);

private:
    QGLShaderProgram *shader;
    Model *model;
    Camera *camera;
    Illumination *illum;

    Point2D posCam;

    void initializeShaders(QString filename);
    void releaseAllShaders();

};

#endif  /* _GLWIDGET_H */
